<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class RegisterApproved extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.register-approved')
            ->subject('ได้สิทธิ์รับบัตรเข้าชม The 1st Real Standup Comedy Night " ด้นคนเดียวกับเรื่องเรียลเรียลของชีวิต" ตอน.... ตลก ขบ คิด ชีวิต ตาหลก แสรดด')
            ->with([
                'firstname' => $this->data['firstname'],
                'lastname' => $this->data['lastname'],
                'ticketCode' => $this->data['ticketCode']
            ]);
    }
}
