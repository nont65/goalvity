<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MaintainanceController extends Controller
{
  public function upgradeData() {
    set_time_limit(0);

    Schema::rename('users', 'old_users');
    Schema::create('users', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('email');
        $table->unsignedTinyInteger('user_type');
        $table->unsignedBigInteger('last_comment_id')->nullable();
        $table->dateTime('last_comment_time')->nullable();
        $table->unsignedBigInteger('recent_view_lesson_id')->nullable();
        $table->dateTime('recent_view_lesson_time')->nullable();
        $table->string('profile_image');
        $table->timestamps();
    });
    Schema::create('user_fb_mapping', function (Blueprint $table) {
        $table->unsignedBigInteger('user_id');
        $table->unsignedBigInteger('fb_id');
        $table->timestamp('created_at');

        $table->primary(['user_id', 'fb_id']);
    });

    Schema::rename('users_lessons', 'old_users_lessons');
    Schema::create('users_lessons', function (Blueprint $table) {
        $table->unsignedBigInteger('user_id');
        $table->unsignedBigInteger('lesson_id');
        $table->timestamp('created_at');

        $table->primary(['user_id', 'lesson_id']);
    });

    $imageExtType = array(
        '1' => 'gif',
        '2' => 'jpg',
        '3' => 'png'
    );

    $oldUserData = DB::table('old_users')
                    ->get();

    foreach ($oldUserData as $user) {
      $oldUserID = $user->id;

      $imageFileName = "";
      $url = "http://graph.facebook.com/" . $oldUserID . "/picture?type=normal";
      $contents = @file_get_contents($url);

      if ($contents) {
          $imageExt = in_array(exif_imagetype($url), $imageExtType) ? $imageExtType[exif_imagetype($url)] : 'jpg';
          $imageFileName = $oldUserID . "." . $imageExt;
          Storage::disk('public_uploads')->put($imageFileName, $contents);
      }

      $checkUserData = DB::table('users')
                      ->where('name', $user->name)
                      ->where('email', $user->email)
                      ->first();
      if (!$checkUserData) {
        $newUserID = DB::table('users')
                        ->insertGetId([
                          'name' => $user->name,
                          'email' => $user->email,
                          'user_type' => $user->user_type,
                          'last_comment_id' => $user->last_comment_id,
                          'last_comment_time' => $user->last_comment_time,
                          'recent_view_lesson_id' => $user->recent_view_lesson_id,
                          'recent_view_lesson_time' => $user->recent_view_lesson_time,
                          'profile_image' => $imageFileName,
                          'created_at' => $user->created_at,
                          'updated_at' => $user->updated_at
                        ]);
      } else {
        $newUserID = $checkUserData->id;

        if ($imageFileName !== "" && $checkUserData->profile_image !== $imageFileName) {
          DB::table('users')
            ->where('id', $newUserID)
            ->update(['profile_image' => $imageFileName]);
        }
      }

      $checkUserMapData = DB::table('user_fb_mapping')
                      ->where('user_id', $newUserID)
                      ->where('fb_id', $oldUserID)
                      ->first();
      if (!$checkUserMapData) {
        DB::table('user_fb_mapping')
          ->insert([
            'user_id' => $newUserID,
            'fb_id' => $oldUserID
          ]);
      }


      // Update Lesson Comment ID
      DB::table('lesson_comments')
            ->where('user_id', $oldUserID)
            ->update(['user_id' => $newUserID]);
      DB::table('lesson_comments')
            ->where('comment_user_id', $oldUserID)
            ->update(['comment_user_id' => $newUserID]);

      // Update Logs Table
      DB::table('logs')
            ->where('user_id', $oldUserID)
            ->update(['user_id' => $newUserID]);

      // Update Users Lesson
      $oldUserLessonData = DB::table('old_users_lessons')
                ->where('user_id', $oldUserID)
                ->get();
      for ($i = 0; $i < count($oldUserLessonData); $i++) {
				$lessonID = $oldUserLessonData[$i]->lesson_id;
        $newUserLessonData = DB::table('users_lessons')
                                ->where('user_id', $newUserID)
                                ->where('lesson_id', $lessonID)
                                ->first();
        if (!$newUserLessonData) {
          DB::table('users_lessons')
            ->insert([
                'user_id' => $newUserID,
                'lesson_id' => $lessonID,
                'created_at' => $oldUserLessonData[$i]->created_at
            ]);
        }
      }
    }

    echo "Done";
  }
}
