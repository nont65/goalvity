<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function getCountStudent() {
        $data = DB::table('users')
                    ->select(DB::raw('count(*) total'))
                    ->where('user_type', 0)
                    ->first();

        return response()->json([ 'success' => 1, 'data' => $data->total ]);
    }

    public function getListStudent(Request $request) {
        $query = DB::table('users')
                    ->select(DB::raw('count(*) total'))
                    ->where('user_type', 0);

        if ($request->input('searchText') != "") {
            $query->where(function($condition) use ($request) {
                $condition->where("name", 'like', '%' . $request->input('searchText') . '%');
            });
        }

        $totalData = $query->first();

        $total = $totalData->total;
        $limit = $request->input('limit');
        $offset = $request->input('offset');

        if ($offset >= $total) {
            $offset = (($offset / $limit) - 1) * $limit;
        }

        $query = DB::table('users')
                    ->leftJoin('lessons', 'users.recent_view_lesson_id', '=', 'lessons.id')
                    ->leftJoin(DB::raw('(SELECT user_id, count(*) count_comment FROM lesson_comments group by user_id) comments'), function($join) {
                        $join->on('users.id', '=', 'comments.user_id');
                    })
                    ->select(DB::raw('users.*, ifnull(lessons.title,"-") title, lessons.alias, ifnull(count_comment, 0) count_comment'))
                    ->where('user_type', 0);

        if ($request->input('searchText') != "") {
            $query->where(function($condition) use ($request) {
                $condition->where("name", 'like', '%' . $request->input('searchText') . '%');
            });
        }

        $data = $query->offset($offset)
                    ->limit($limit)
                    ->orderBy($request->input('sort'), $request->input('order'))
                    ->get();

        $result = [
            'total' => $total,
            'offset' => $offset,
            'limit' => $limit,
            'data' => $data
        ];

        return response()->json([ 'success' => 1, 'data' => $result ]);
    }
}
