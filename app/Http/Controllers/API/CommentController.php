<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    public function getComments($user_id, $last_comment_id) {
        $takeData = $last_comment_id != 0 ? 10 : 5;

        $totalRemainQuery = DB::table('lesson_comments')
                        ->select(DB::raw('count(*) total'))
                        ->where('user_id', $user_id)
                        ->where('parent_id', 0);
        if ($last_comment_id != 0) {
            $totalRemainQuery->where('id', '<', $last_comment_id);
        }
        $totalRemainData = $totalRemainQuery->first();
        if ($last_comment_id == 0)  {
            $tmpTotalRemainData = $totalRemainData->total - 5;
            if ($tmpTotalRemainData < 0) {
                $tmpTotalRemainData = 0;
            }
            $totalRemainData = $tmpTotalRemainData > 10 ? 10 : $tmpTotalRemainData;
        } else {
            $totalRemainData = $totalRemainData->total > 10 ? 10 : $totalRemainData;
        }

        $query = DB::table('lesson_comments')
                    ->join('users', 'lesson_comments.comment_user_id', '=', 'users.id')
                    ->join('lessons', 'lesson_comments.lesson_id', '=', 'lessons.id')
                    ->select(DB::raw('lesson_comments.*, users.name, users.user_type, lessons.title lesson_title, profile_image'))
                    ->where('user_id', $user_id)
                    ->where('parent_id', 0);
        if ($last_comment_id != 0) {
            $query->where('lesson_comments.id', '<', $last_comment_id);
        }
        $data = $query->take($takeData)
                    ->orderBy('created_at', 'desc')
                    ->get();

        foreach ($data as $index => $item) {
            $totalAnswerData = DB::table('lesson_comments')
                                    ->select(DB::raw('count(*) total_answer'))
                                    ->where('parent_id', $item->id)
                                    ->first();
            $data[$index]->more_answers = $totalAnswerData->total_answer > 1;
            $data[$index]->profile_image = getProfileImageURL($data[$index]->profile_image);
            $data[$index]->answers = DB::table('lesson_comments')
                                        ->join('users', 'lesson_comments.comment_user_id', '=', 'users.id')
                                        ->select(DB::raw('lesson_comments.*, users.name, users.user_type, profile_image'))
                                        ->where('parent_id', $item->id)
                                        ->take(1)
                                        ->orderBy('created_at', 'desc')
                                        ->get();
            foreach ($data[$index]->answers as $answerIndex => $answer) {
                $data[$index]->answers[$answerIndex]->profile_image = getProfileImageURL($answer->profile_image);
            }
        }

        return response()->json([ 'success' => 1, 'remain_data'=> $totalRemainData, 'data' => $data ]);
    }

    public function getAnswerComments($comment_id) {
        $data = DB::table('lesson_comments')
                    ->join('users', 'lesson_comments.comment_user_id', '=', 'users.id')
                    ->select(DB::raw('lesson_comments.*, users.name, users.user_type, profile_image'))
                    ->where('parent_id', $comment_id)
                    ->orderBy('created_at', 'asc')
                    ->get();
        foreach ($data as $index => $answer) {
            $data[$index]->profile_image = getProfileImageURL($answer->profile_image);
        }
        return response()->json([ 'success' => 1, 'data' => $data ]);
    }

    public function postComment(Request $request) {
        $validator = Validator::make($request->all(), [
            'lesson_id' => 'required',
            'user_id' => 'required',
            'comment_user_id' => 'required',
            'content' => 'required',
            'parent_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => 0, 'message' => $validator->errors()->first() ]);
        }

        $lessonCommentsData = [
            'lesson_id' => $request->input('lesson_id'),
            'user_id' => $request->input('user_id'),
            'comment_user_id'  => $request->input('comment_user_id'),
            'content' => $request->input('content'),
            'parent_id' => $request->input('parent_id')
        ];

        try {
            $comment_id = DB::table('lesson_comments')
                            ->insertGetId($lessonCommentsData);

            DB::table('lessons')
                ->where('id', $request->input('lesson_id'))
                ->update(['last_comment_id' => $comment_id, 'last_comment_time' => date('Y-m-d H:i:s')]);

            DB::table('users')
                ->where('id', $request->input('user_id'))
                ->update([
                    'last_comment_id' => $comment_id,
                    'last_comment_time' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

            $userData = DB::table('users')
                ->where('id', $request->input('comment_user_id'))
                ->first();

            $lessonCommentsData['id'] = $comment_id;
            $lessonCommentsData['name'] = $userData->name;
            $lessonCommentsData['user_type'] = $userData->user_type;
            $lessonCommentsData['profile_image'] = getProfileImageURL($userData->profile_image);
            $lessonCommentsData['more_answers'] = false;
            $lessonCommentsData['answers'] = [];
            $lessonCommentsData['created_at'] = date('Y-m-d H:i:s');

            if ($request->input('parent_id') == 0) {
                $lessonData = DB::table('lessons')
                                ->select(DB::raw('title'))
                                ->where('id', $request->input('lesson_id'))
                                ->first();
                if ($lessonData) {
                    $lessonCommentsData['lesson_title'] = $lessonData->title;
                } else {
                    $lessonCommentsData['lesson_title'] = '';
                }
            }

            return response()->json([ 'success' => 1, 'data' => $lessonCommentsData ]);
        } catch (\Illuminate\Database\QueryException $error) {
            dd($error);
            return response()->json([ 'success' => 0, 'message' => 'add comment failed' ]);
        }
    }
}
