<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Mail\RegisterApproved;
use App\Mail\ThanksEmail;
use Keygen\Keygen;
use QRCode;

class EventController extends Controller
{
    public function getCount() {
        $data = DB::table('event_surveys')
                    ->select(DB::raw('count(*) total, sum(case when approve_status = 0 then 0 else 1 end) total_approved'))
                    ->first();

        return response()->json([ 'success' => 1, 'data' => $data ]);
    }

    public function getListRegistered(Request $request) {
        $query = DB::table('event_surveys')
                    ->select(DB::raw('count(*) total'));

        if ($request->input('searchText') != "") {
            $query->where(function($query) use ($request) {
                $query->where("firstname", "like", '%' . $request->input('searchText') . '%')
                    ->orWhere("lastname", "like", '%' . $request->input('searchText') . '%')
                    ->orWhere("tel", "like", $request->input('searchText') . '%');
            });
        }

        $totalData = $query->first();

        $total = $totalData->total;
        $limit = $request->input('limit');
        $offset = $request->input('offset');

        if ($offset >= $total) {
            $offset = (($offset / $limit) - 1) * $limit;
        }

        $query = DB::table('event_surveys')
                    ->select(DB::raw('id, user_id, concat(firstname, " ", lastname)  fullname, firstname, lastname, nickname, email, tel, occupation, description, approve_status'));

        if ($request->input('searchText') != "") {
            $query->where(function($query) use ($request) {
                $query->where("firstname", "like", '%' . $request->input('searchText') . '%')
                    ->orWhere("lastname", "like", '%' . $request->input('searchText') . '%')
                    ->orWhere("tel", "like", $request->input('searchText') . '%');
            });
        }

        $data = $query->offset($offset)
                    ->limit($limit)
                    ->orderBy($request->input('sort'), $request->input('order'))
                    ->get();

        $result = [
            'total' => $total,
            'offset' => $offset,
            'limit' => $limit,
            'data' => $data
        ];

        return response()->json([ 'success' => 1, 'data' => $result ]);
    }

    public function setApprovedRegistered(Request $request) {
        $eventSurveyData = DB::table('event_surveys')
                    ->where('id', $request->input('id'))
                    ->select(DB::raw('user_id, firstname, lastname, email, tel'))
                    ->first();

        $ticketCode = Keygen::numeric(12)->generate();

        $qrCodeData = json_encode(array(
            'ticket_code' => $ticketCode,
            'fullname' => $eventSurveyData->firstname . ' ' .$eventSurveyData->lastname,
            'tel' => $request->input('tel')
        ));

        QRCode::text($qrCodeData)->setOutfile(public_path() . '/tickets/' . $ticketCode . '.png')->png();

        try {
            DB::table('event_surveys')
                ->where('id', $request->input('id'))
                ->update(['approve_status' => 1]);
        } catch (\Illuminate\Database\QueryException $error) {
            return response()->json([ 'success' => 0, 'message' => 'cannot approved registered', 'error' => $error ]);
        }

        try {
            DB::table('event_tickets')
                ->insert([
                    'survey_id' => $request->input('id'),
                    'user_id' => $eventSurveyData->user_id,
                    'ticket_code' => $ticketCode,
                    'qrcode_data' => $qrCodeData,
                    'status' => 0,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
        } catch (\Illuminate\Database\QueryException $error) {
            return response()->json([ 'success' => 0, 'message' => 'cannot insert event ticket', 'error' => $error ]);
        }

        $this->sendApproveMail($eventSurveyData->firstname, $eventSurveyData->lastname, $eventSurveyData->email, $ticketCode);

        return response()->json([ 'success' => 1 ]);
    }

    public function resendEmail(Request $request) {
        $eventSurveyData = DB::table('event_surveys')
                    ->where('id', $request->input('id'))
                    ->select(DB::raw('user_id, firstname, lastname, email, tel'))
                    ->first();

        $eventTicketData = DB::table('event_tickets')
                        ->where('survey_id', $request->input('id'))
                        ->select(DB::raw('ticket_code'))
                        ->first();

        $this->sendApproveMail($eventSurveyData->firstname, $eventSurveyData->lastname, $eventSurveyData->email, $eventTicketData->ticket_code);

        return response()->json([ 'success' => 1 ]);
    }

    public function useTicket(Request $request) {
        $testTicketCode = ['846512906211','959226984832','755868200190'];
        if (in_array($request->input('ticket'), $testTicketCode) > 0) {
            return response()->json([ 'success' => 1, 'message' => 'Change ticket status successful' ]);
        }

        $data = DB::table('event_tickets')
                    ->select(DB::raw('survey_id ,status, used_at'))
                    ->where('ticket_code', $request->input('ticket'))
                    ->first();
        if (!$data) {
            return response()->json([ 'success' => 0, 'message' => 'Ticket is not found' ]);
        }

        if ($data->status === 1) {
            return response()->json([ 'success' => 0, 'message' => 'Ticket has already used at ' . $data->used_at ]);
        }

        try {
            DB::table('event_tickets')
                ->where('ticket_code', $request->input('ticket'))
                ->update(['status' => 1, 'used_at' => date('Y-m-d H:i:s')]);
        } catch (\Illuminate\Database\QueryException $error) {
            return response()->json([ 'success' => 0, 'message' => 'Cannot update ticket\'s status' ]);
        }

        try {
            DB::table('event_surveys')
                ->where('id', $data->survey_id)
                ->update(['approve_status' => 2]);

        } catch (\Illuminate\Database\QueryException $error) {
            return response()->json([ 'success' => 0, 'message' => 'Cannot update register\'s approve status' ]);
        }

        return response()->json([ 'success' => 1, 'message' => 'Change ticket status successful' ]);
    }

    private function sendApproveMail($firstname, $lastname, $email, $ticketCode) {
        $data = array(
            'firstname' => $firstname,
            'lastname' => $lastname,
            'ticketCode' => $ticketCode,
        );
        Mail::to($email)
            ->send(new RegisterApproved($data));
    }

    public function sendThankMail($skip) {
      // $data = array(
      //     'firstname' => 'test',
      //     'lastname' => 'lastname',
      //     'ticketCode' => '1234',
      // );



      $data = DB::table('event_surveys')
                  ->where('approve_status', 2)
                  ->where('email', '!=', 'kaed@pixelone.co.th')
                  ->skip(10 * $skip)
                  ->take(10)
                  ->get();


      foreach ($data as $value) {
        if ($value->email != 'kaed@pixelone.co.th') {
          Mail::to($value->email)->send(new ThanksEmail($data));
        }
      }


      return response()->json([ 'success' => 1 ]);
    }
}
