<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => 0, 'message' => $validator->errors()->first() ]);
        }

        $fbID = $request->get('id');
        $email = $request->get('email') ? $request->get('email') : '';
        $name = $request->get('name');

        $url = "http://graph.facebook.com/" . $fbID . "/picture?type=normal";
        $imageExtType = array(
            '1' => 'gif',
            '2' => 'jpg',
            '3' => 'png'
        );
        $imageFileName = "";
        $contents = @file_get_contents($url);
        if ($contents) {
            $imageExt = in_array(exif_imagetype($url), $imageExtType) ? $imageExtType[exif_imagetype($url)] : 'jpg';
            $imageFileName = $fbID . "." . $imageExt;
            Storage::disk('public_uploads')->put($imageFileName, $contents);
        }

        try {
            $fbUserData = DB::table('user_fb_mapping')
                                    ->join('users', 'user_fb_mapping.user_id', '=', 'users.id')
                                    ->leftJoin(DB::raw('(SELECT id, alias FROM lessons WHERE status = 1) active_lessons'), function($join) {
                                        $join->on('users.recent_view_lesson_id', '=', 'active_lessons.id');
                                    })
                                    ->select(DB::raw('users.id, email, name, user_type, recent_view_lesson_id "lesson_id", alias "lesson_alias", profile_image'))
                                    ->where('fb_id', $fbID)
                                    ->first();
            if (is_null($fbUserData)) {
                $userData = DB::table('users')
                                ->leftJoin(DB::raw('(SELECT id, alias FROM lessons WHERE status = 1) active_lessons'), function($join) {
                                    $join->on('users.recent_view_lesson_id', '=', 'active_lessons.id');
                                })
                                ->select(DB::raw('users.id, email, name, user_type, recent_view_lesson_id "lesson_id", alias "lesson_alias", profile_image'))
                                ->where('name', $name)
                                ->where('email', $email)
                                ->first();
                if (is_null($userData)) {
                    /* Insert User Data */
                    $userId = DB::table('users')->insertGetId([
                                'email' => $email,
                                'name' => $name,
                                'profile_image' => $imageFileName,
                                'user_type' => 0,
                                'created_at' => date('Y-m-d H:i:s')
                            ]);

                    /* Insert User Mapping */
                    DB::table('user_fb_mapping')
                        ->insert([
                            'user_id' => $userId,
                            'fb_id' => $fbID,
                            'created_at' => date('Y-m-d H:i:s')
                        ]);

                    $resultData = [
                        'id' => $userId,
                        'email' => $email,
                        'name' => $name,
                        'profile_image' => $imageFileName,
                        'user_type' => 0,
                        'lesson_id' => NULL,
                        'lesson_alias' => NULL
                    ];
                } else {
                    /* Insert User Mapping */
                    DB::table('user_fb_mapping')
                        ->insert([
                            'user_id' => $userData->id,
                            'fb_id' => $fbID,
                            'created_at' => date('Y-m-d H:i:s')
                        ]);

                    $resultData =  (array) $userData;
                }
            } else {
                $userId = $fbUserData->id;

                /* Update Data */
                if ($email != $fbUserData->email || $name != $fbUserData->name || ($imageFileName != "" && $imageFileName != $fbUserData->profile_image)) {
                    DB::table('users')
                        ->where('id', $userId)
                        ->update([
                            'email' => $email,
                            'name' => $name,
                            'profile_image' => $imageFileName,
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                }

                $resultData =  (array) $fbUserData;
                $resultData['email'] = $email;
                $resultData['name'] = $name;
            }
            $resultData['profile_image'] = getProfileImageURL($imageFileName);

            if ($resultData['user_type'] === 0) {
                if (is_null($resultData['lesson_alias'])) {
                    $lessonData = DB::table('lessons')
                                    ->select(DB::raw('id, alias'))
                                    ->where('status', 1)
                                    ->orderBy('created_at', 'desc')
                                    ->first();
                    if ($lessonData) {
                        $resultData['lesson_id'] = $lessonData->id;
                        $resultData['lesson_alias'] = $lessonData->alias;

                        $userLessonData = DB::table('users_lessons')
							->select(DB::raw('count(*) count_data'))
							->where('user_id', $userId)
							->where('lesson_id', $lessonData->id)
							->first();

						if ($userLessonData->count_data == 0) {
							DB::table('users_lessons')->insert([
								'user_id' => $userId,
								'lesson_id' => $lessonData->id
							]);
						}
                    }
                }

                if (is_null($resultData['lesson_alias'])) {
                    return response()->json([ 'success' => 0, 'message' => 'Get Lesson Data failed' ]);
                }
            }

            return response()->json([ 'success' => 1, 'data' => $resultData ]);
        } catch (\Illuminate\Database\QueryException $error) {
            dd($error);
            return response()->json([ 'success' => 0, 'message' => 'Upsert User Data failed' ]);
        }
    }

    public function checkUserLesson($user_id, $lesson_id) {
        $userData = $this->getUserData($user_id);
        if (!$userData) {
            return response()->json([ 'success' => 0, 'data' => 'user not found' ]);
        }

        $userLessonData = DB::table('users_lessons')
                        ->select(DB::raw('count(*) count_data'))
                        ->where('user_id', $user_id)
                        ->where('lesson_id', $lesson_id)
                        ->first();
        if (is_null($userLessonData)) {
            return response()->json([ 'success' => 1, 'data' => 0 ]);
        }

        return response()->json([ 'success' => 1, 'data' => $userLessonData->count_data ]);
    }

    public function addUserLesson(Request $request, $user_id) {
        $validator = Validator::make($request->all(), [
            'lesson' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => 0, 'message' => $validator->errors()->first() ]);
        }

        $userData = $this->getUserData($user_id);
        if (!$userData) {
            return response()->json([ 'success' => 0, 'data' => 'user not found' ]);
        }

        $lesson = $request->get('lesson');

        $lessonData = DB::table('lessons')
                    ->select(DB::raw('id'))
                    ->where('alias', $lesson)
                    ->where('status', 1)
                    ->first();
        if (is_null($lessonData)) {
            return response()->json([ 'success' => 0, 'message' => 'Lesson not found' ]);
        }

        $userLessonData = DB::table('users_lessons')
                ->select(DB::raw('count(*) count_data'))
                ->where('user_id', $user_id)
                ->where('lesson_id', $lessonData->id)
                ->first();
        if (is_null($userLessonData)) {
            return response()->json([ 'success' => 0, 'message' => 'User - Lesson error' ]);
        }
        if ($userLessonData->count_data == 0) {
            DB::table('users_lessons')->insert([
                'user_id' => $user_id,
                'lesson_id' => $lessonData->id
            ]);
        }

        DB::table('users')
            ->where('id', $user_id)
            ->update([
                'recent_view_lesson_id' => $lessonData->id,
                'recent_view_lesson_time' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')]
            );

        DB::table('lessons')
            ->where('id', $request->input('lesson_id'))
            ->increment('view_count');

        $logID = date('YmdHis') . rand(10000, 99999);
        DB::table('logs')
            ->insert([
                'user_id' => $user_id,
                'log_id' => $logID,
                'action' => 'open-lesson',
                'detail' => '{"lesson_id":' . $lessonData->id . '}'
            ]);

        return response()->json([ 'success' => 1 ]);
    }

    public function getUserLesson(Request $request, $user_id) {
        $userData = $this->getUserData($user_id);
        if (!$userData) {
            return response()->json([ 'success' => 0, 'data' => 'user not found' ]);
        }

        if ($userData->user_type == 0) {
            $userLessonData = DB::table('users_lessons')
                    ->join('lessons', 'users_lessons.lesson_id', '=', 'lessons.id')
                    ->select(DB::raw('lesson_id, alias'))
                    ->where('user_id', $user_id)
                    ->where('lessons.status', 1)
                    ->orderBy('users_lessons.created_at', 'desc')
                    ->first();
            if (!$userLessonData) {
                $lessonData = DB::table('lessons')
                    ->select(DB::raw('id, alias'))
                    ->where('lessons.status', 1)
                    ->orderBy('created_at', 'desc')
                    ->first();
                if (!$lessonData) {
                    return response()->json([ 'success' => 0 ]);
                }

                try {
                    DB::table('users_lessons')->insert([
                        'user_id' => $user_id,
                        'lesson_id' => $lessonData->id
                    ]);
                } catch (\Illuminate\Database\QueryException $error) {}

                $alias = $lessonData->alias;
            } else {
                $alias = $userLessonData->alias;
            }
        } else {
            $lessonData = DB::table('lessons')
                ->select(DB::raw('id, alias'))
                ->where('lessons.status', 1)
                ->orderBy('created_at', 'desc')
                ->first();
            if (!$lessonData) {
                return response()->json([ 'success' => 1, 'data' => '' ]);
            }

            $alias = $lessonData->alias;
        }

        return response()->json([ 'success' => 1, 'data' => $alias ]);
    }

    public function updateRecentViewLessonId(Request $request, $user_id) {
        $validator = Validator::make($request->all(), [
            'lesson_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => 0, 'message' => $validator->errors()->first() ]);
        }

        try {
            DB::table('users')
                ->where('id', $user_id)
                ->update([
                    'recent_view_lesson_id' => $request->input('lesson_id'),
                    'recent_view_lesson_time' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')]
                );

            DB::table('lessons')
                ->where('id', $request->input('lesson_id'))
                ->increment('view_count');

            $logID = date('YmdHis') . rand(10000, 99999);
            DB::table('logs')
                ->insert([
                    'user_id' => $user_id,
                    'log_id' => $logID,
                    'action' => 'open-lesson',
                    'detail' => '{"lesson_id":' . $request->input('lesson_id') . '}'
                ]);

            return response()->json([ 'success' => 1 ]);
        } catch (\Illuminate\Database\QueryException $error) {
            return response()->json([ 'success' => 0, 'message' => 'Update recent view lesson id failed' ]);
        }
    }

    public function getUserDetail($user_id) {
        $userData = $this->getUserData($user_id);

        if (!$userData) {
            return response()->json([ 'success' => 0, 'message' => 'User not found' ]);
        }

        return response()->json([ 'success' => 1, 'data' => $userData ]);
    }

    public function getEventRegister($user_id) {
        try {
            $registerEventData = DB::table('event_surveys')
                ->select(DB::raw('firstname, lastname, nickname, email, tel, occupation, description'))
                ->where('user_id', $user_id)
                ->first();
            if ($registerEventData) {
                return response()->json([ 'success' => 1, 'data' => $registerEventData ]);
            } else {
                return response()->json([ 'success' => 1, 'data' => null ]);
            }
        } catch (\Illuminate\Database\QueryException $error) {
            return response()->json([ 'success' => 0, 'message' => 'get register event failure' ]);
        }
    }

    public function submitEventRegister(Request $request) {
        try {
            $registerEventData = DB::table('event_surveys')
                ->select(DB::raw('count(*) count_data'))
                ->where('user_id', $request->input('user_id'))
                ->first();
            if ($registerEventData->count_data > 0) {
                return response()->json([ 'success' => 0, 'message' => 'submit register event failure', 'error' => 'duplicated user id' ]);
            }

            DB::table('event_surveys')
                ->insert([
                    'user_id' => $request->input('user_id'),
                    'firstname' => $request->input('firstname'),
                    'lastname' => $request->input('lastname'),
                    'nickname' => $request->input('nickname'),
                    'email' => $request->input('email'),
                    'tel' => $request->input('tel'),
                    'occupation' => $request->input('occupation'),
                    'description' => $request->input('description'),
                    'approve_status' => 0
                ]);

            return response()->json([ 'success' => 1, 'message' => 'submit register event successful' ]);
        } catch (\Illuminate\Database\QueryException $error) {
            return response()->json([ 'success' => 0, 'message' => 'submit register event failure', 'error' => $error ]);
        }
    }

    private function getUserData($user_id) {
        $userData = DB::table('users')
                ->where('id', $user_id)
                ->first();
        return $userData;
    }
}
