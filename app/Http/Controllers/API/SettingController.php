<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function getConfigSettings() {
        $settingData = DB::table('settings')
                    ->select(DB::raw('setting_field_name, value'))
                    ->get();
        if (!$settingData) {
            return response()->json([ 'success' => 0, 'message' => 'setting not found' ]);
        }

        $resultData = [];
        foreach ($settingData as $setting) {
            $resultData[$setting->setting_field_name] = $setting->value;
        }

        return response()->json([ 'success' => 1, 'data' => $resultData ]);
    }

    public function save(Request $request) {
        try {
            foreach ($request->all() as $field => $value) {
                DB::table('settings')
                    ->where('setting_field_name', $field)
                    ->update([
                        'value' => $value
                    ]);
            }
            return response()->json([ 'success' => 1 ]);
        } catch (\Illuminate\Database\QueryException $error) {
            return response()->json([ 'success' => 0, 'message' => 'update settings failed' ]);
        }
    }
}
