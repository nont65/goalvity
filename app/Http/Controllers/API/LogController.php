<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class LogController extends Controller
{
    public function addLog(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'action' => 'required',
            'detail' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => 0, 'message' => $validator->errors()->first() ]);
        }

        $logID = date('YmdHis') . rand(10000, 99999);
        try {
            DB::table('logs')
                ->insert([
                    'user_id' => $request->input('user_id'),
                    'log_id' => $logID,
                    'action' => $request->input('action'),
                    'detail' => $request->input('detail')
                ]);
            return response()->json([ 'success' => 1 ]);
        } catch (\Illuminate\Database\QueryException $error) {
            return response()->json([ 'success' => 0, 'message' => 'add log failed' ]);
        }

    }
}
