<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class LessonController extends Controller
{
    public function getOpenLesson($alias) {
        $lessonData = DB::table('lessons')
                    ->where('alias', $alias)
                    ->where('status', 1)
                    ->first();
        if (!$lessonData) {
            return response()->json([ 'success' => 0, 'message' => 'lesson not found' ]);
        }

        return response()->json([ 'success' => 1, 'data' => $lessonData ]);
    }

    public function getLesson($alias) {
        $lessonData = DB::table('lessons')
                    ->where('alias', $alias)
                    ->first();
        if (!$lessonData) {
            return response()->json([ 'success' => 0, 'message' => 'lesson not found' ]);
        }

        return response()->json([ 'success' => 1, 'data' => $lessonData ]);
    }

    public function getCountLesson() {
        return response()->json([ 'success' => 1, 'data' => $this->getTotalLesson() ]);
    }

    public function getListLesson(Request $request) {
        $total = $this->getTotalLesson();
        $limit = $request->input('limit');
        $offset = $request->input('offset');

        if ($offset >= $total) {
            $offset = (($offset / $limit) - 1) * $limit;
        }

        $data = DB::table('lessons')
                    ->offset($offset)
                    ->limit($limit)
                    ->orderBy($request->input('sort'), $request->input('order'))
                    ->get();

        $result = [
            'total' => $total,
            'offset' => $offset,
            'limit' => $limit,
            'data' => $data
        ];

        return response()->json([ 'success' => 1, 'data' => $result ]);
    }

    public function getLessonAlias() {
        $alias = $this->genLessonAlias();

        if ($alias == '') {
            return response()->json([ 'success' => 0 ]);
        }
        return response()->json([ 'success' => 1, 'data' => $alias]);
    }

    public function add(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'alias' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => 0, 'message' => $validator->errors()->first() ]);
        }

        $aliasData = DB::table('lessons')
                        ->where('alias', $request->input('alias'))
                        ->first();
        if ($aliasData) {
            $alias = $this->genLessonAlias();
        } else {
            $alias = $request->input('alias');
        }

        try {
            $id = DB::table('lessons')
                    ->insertGetId([
                        'title' => $request->input('title'),
                        'alias' => $alias,
                        'status' => $request->input('status'),
                        'youtube_url' => $request->input('youtube_url'),
                        'description' => $request->input('description')
                    ]);
            return response()->json([ 'success' => 1, 'id' => $id, 'alias' => $alias ]);
        } catch (\Illuminate\Database\QueryException $error) {
            return response()->json([ 'success' => 0, 'message' => 'add lessons failed' ]);
        }
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'alias' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'success' => 0, 'message' => $validator->errors()->first() ]);
        }

        try {
            DB::table('lessons')
                ->where('id', $id)
                ->update([
                    'title' => $request->input('title'),
                    'alias' => $request->input('alias'),
                    'status' => $request->input('status'),
                    'youtube_url' => $request->input('youtube_url'),
                    'description' => $request->input('description')
                ]);
            return response()->json([ 'success' => 1 ]);
        } catch (\Illuminate\Database\QueryException $error) {
            return response()->json([ 'success' => 0, 'message' => 'update lessons failed' ]);
        }
    }

    private function getTotalLesson() {
        $data = DB::table('lessons')
                ->select(DB::raw('count(*) total'))
                ->first();
        if ($data) {
            return $data->total;
        }

        return 0;
    }

    private function genLessonAlias() {
        try {
            $data = DB::table('INFORMATION_SCHEMA.TABLES')
                        ->select(DB::raw('AUTO_INCREMENT'))
                        ->where('TABLE_SCHEMA', getenv('DB_DATABASE'))
                        ->where('TABLE_NAME', 'lessons')
                        ->first();
            return str_pad($data->AUTO_INCREMENT, 3, '0', STR_PAD_LEFT);
        } catch (\Illuminate\Database\QueryException $error) {
            return '';
        }
    }
}
