<?php

function getProfileImageURL($profileImageFilename) {
    if (trim($profileImageFilename) == "") {
        return getenv("APP_URL") . "/imgs/dummy.jpg";
    }

    return getenv("APP_URL") . "/imgs/profiles/" . $profileImageFilename . "?v=" . rand(0,999);
}
