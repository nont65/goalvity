<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Goalvity</title>
    <link href="{{asset('css/app.css')}}?v={{ rand(0,10000) }}" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div id="app"></div>
	<script>
        window.BaseURL = '{{ env('APP_URL') }}'
        window.BasePath = '/'
        window.APIPath = "{{ env('APP_URL') }}/api";
        // window.PublicPath = "{{asset('')}}";
        // window.BackendPath = "{{asset('backend')}}";
        window.fbAppID = '{{ env('FB_APP_ID') }}';
        window.fbAppVersion = '{{ env('FB_APP_VERSION') }}';
    </script>
    <script src="{{ asset('js/app.js') }}?v={{ rand(0,10000) }}" type="text/javascript"></script>
  </body>
</html>
