export default {
  /* HeaderSettings/index.vue */
  // 'Apply': '应用', 'Apply and backup settings to local': '应用并保存设置到本地', 'Clear
  // local settings backup and restore': '清空本地设置并恢复', 'Using local settings':
  // '正在使用本地设置',

  /* Table/TableBody.vue */
  'No Data' : 'ไม่พบข้อมูล',

  /* index.vue */
  'Total' : 'ข้อมูลทั้งหมด',
  ',' : 'รายการ แสดง',

  /* PageSizeSelect.vue */
  'items / page' : 'รายการ'
}
