import Vue from 'vue'
import VueRouter from 'vue-router';
import axios from 'axios';
import swal from 'sweetalert2';
import InApp from 'detect-inapp';

import {getAsync} from '../utils';
import store from '../store';

import {Landing} from '../views/landing';
import {Lesson} from '../views/lesson';
import {EventPage} from '../views/event-page';

import {StudentList} from '../views/student-list';
import {LessonAnswer} from '../views/lesson-answer';
import {LessonList} from '../views/lesson-list';
import {LessonForm} from '../views/lesson-form';
import {EventList} from '../views/event-list';
import {SettingForm} from '../views/setting-form';
import {Page404} from '../views/404';

Vue.use(VueRouter);

const router = new VueRouter({
  base: window.BasePath,
  mode: 'history',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({y: 0}),
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing
    }, {
      path: '/lesson/:alias',
      name: 'Lesson',
      component: Lesson,
      beforeEnter: async(to, from, next) => {
        if (window.entryUser) {
          let { result: loginResult, error: loginError } = await getAsync(
            axios.post("/login", window.entryUser)
          );

          if (loginError || loginResult.data.success === 0) {
            swal({
              title: "ไม่สามารถเข้าสู่ระบบได้",
              type: "error"
            });
            return;
          }

          const userData = loginResult.data.data;
          store.dispatch('setUserData', userData);

          if (userData.user_type === 1) {
            next('/students')
          } else {
            next()
          }
        } else {
          next();
        }
      }
    }, {
      path: '/the-first-real-standup-comedy-night',
      name: 'EventPage',
      component: EventPage,
      beforeEnter: async(to, from, next) => {
        if (window.entryUser) {
          let { result: loginResult, error: loginError } = await getAsync(
            axios.post("/login", window.entryUser)
          );

          if (loginError || loginResult.data.success === 0) {
            swal({
              title: "ไม่สามารถเข้าสู่ระบบได้",
              type: "error"
            });
            return;
          }

          const userData = loginResult.data.data;
          store.dispatch('setUserData', userData);

          if (userData.user_type === 1) {
            next('/events')
          } else {
            next()
          }
        } else {
          next();
        }
      }
    }, {
      path: '/students',
      name: 'StudentList',
      component: StudentList,
      meta: {
        adminAuth: true
      }
    }, {
      path: '/lesson-answer/:alias/student/:student',
      name: 'LessonAnswer',
      component: LessonAnswer,
      meta: {
        adminAuth: true
      },
      beforeEnter: async(to, from, next) => {
        let {result: getLessonResult, error: getLessonError} = await getAsync(axios.get(`/lessons/${to.params.alias}`));
        if (getLessonError || getLessonResult.data.success === 0) {
          next('/404');
        } else {
          let {result: getUserResult, error: getUserError} = await getAsync(axios.get(`/users/${to.params.student}`));
          if (getUserError || getLessonResult.data.success === 0) {
            next('/404');
          } else {
            next();
          }
        }
      }
    }, {
      path: '/lessons',
      name: 'LessonList',
      component: LessonList,
      meta: {
        adminAuth: true
      }
    }, {
      path: '/lessons/new',
      name: 'LessonNew',
      component: LessonForm,
      meta: {
        adminAuth: true
      }
    }, {
      path: '/lessons/edit/:alias',
      name: 'LessonEdit',
      component: LessonForm,
      meta: {
        adminAuth: true
      }
    }, {
        path: '/events',
        name: 'EventList',
        component: EventList,
        meta: {
          adminAuth: true
        }
    }, {
        path: '/setting',
        name: 'SettingForm',
        component: SettingForm,
        meta: {
          adminAuth: true
        }
    }, {
      path: '/404',
      name: 'NotFound',
      component: Page404
    }, {
      path: '*',
      redirect: {
        name: 'NotFound'
      }
    }
  ]
});

router.beforeEach(async(to, from, next) => {
  const inapp = new InApp(navigator.userAgent || navigator.vendor || window.opera);
  if (inapp.isInApp) {
    // Show Response in App Popup
    store.dispatch('showResponseInApp', true)
  } else {
    if (to.matched.some(record => record.meta.adminAuth)) {
      if (!window.entryUser) {
        next('/');
      } else {
        let userData;
        if (store.state.users.userData) {
          userData = store.state.users.userData;
        } else {
          let {result: loginResult, error: loginError} = await getAsync(axios.post('/login', window.entryUser));
          if (loginError) {
            swal({title: 'ไม่สามารถเข้าสู่ระบบได้ในขณะนี้', type: 'error'});
            next('/');
            return;
          } else {
            userData = loginResult.data.data;
            store.dispatch('setUserData', userData);
          }
        }

        if (userData.user_type === 0) {
          next('/404');
        } else {
          next();
        }
      }
    } else {
      next();
    }
  }
});

export default router;
