import Vue from 'vue';
import Vuex from 'vuex';

import {utils, users} from './modules';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    utils,
    users
  },
  strict: true
});
