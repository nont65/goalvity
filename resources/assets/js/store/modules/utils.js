import * as types from '../mutation-types';

// initial state
const state = {
  showLoadingStatus: false,
  showResponseInAppStatus: false
}

// getter
const getters = {
  loadingStatus: state => state.showLoadingStatus,
  responseInAppStatus: state => state.showResponseInAppStatus
};

// action
const actions = {
  showLoading({ commit }, newStatus) {
    commit(types.CHANGE_LOADING_STATUS, newStatus)
  },
  showResponseInApp({ commit }, newStatus) {
    if (newStatus) {
      $('body').css({'overflow': 'hidden'});
    } else {
      $('body').css({'overflow': 'auto'});
    }

    commit(types.CHANGE_SHOW_RESPONSE_IN_APP_STATUS, newStatus)
  }
}

// mutation
const mutations = {
  [types.CHANGE_LOADING_STATUS](state, newStatus) {
    state.showLoadingStatus = newStatus
  },
  [types.CHANGE_SHOW_RESPONSE_IN_APP_STATUS](state, newStatus) {
    state.showResponseInAppStatus = newStatus
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
