import * as types from '../mutation-types';

// initial state
const state = {
  userData: null
};

// getter
const getters = {
  fbUserName: state => (state.userData)
    ? state.userData.name
    : '',
  fbUserImageData: state => (state.userData)
    ? state.userData.profile_image
    : ''
};

// action
const actions = {
  setUserData({
    commit
  }, payload) {
    commit(types.SET_USER_DATA, payload);
  },
  setUserLessonData({
    commit
  }, payload) {
    commit(types.UPDATE_USER_LESSON, payload);
  }
};

// mutation
const mutations = {
  [types.SET_USER_DATA](state, userData) {
    state.userData = userData;
  },
  [types.UPDATE_USER_LESSON](state, lessonData) {
    state.userData = {
      ...state.userData,
      lesson_id: lessonData.id,
      lesson_alias: lessonData.alias
    };
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
