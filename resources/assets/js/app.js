/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import VueTextareaAutosize from 'vue-textarea-autosize';
import Datatable from 'vue2-datatable-component';
import locale from './datatable-locale';
import VueClipboard from 'vue-clipboard2';
import BlockUI from 'vue-blockui';
import Vuelidate from 'vuelidate';

import App from './App.vue';
import store from './store';
import router from './router';
import axios from 'axios';

axios.defaults.baseURL = window.APIPath;

window.fbAsyncInit = function () {
  FB.init({
    appId: window.fbAppID, cookie: true, // enable cookies to allow the server to access the session
    xfbml: true, // parse social plugins on this page
    version: window.fbAppVersion // use graph api version 2.8
  });

  FB.getLoginStatus(function (response) {
    if (response.status === 'connected') {
      FB
        .api('/me', {
          fields: 'id, name, email'
        }, function (res) {
          initApp(res);
        });
    } else {
      initApp(null);
    }
  });

};
(function (d, s, id) {
  var js,
    fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id))
    return;
  js = d.createElement(s);
  js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs
    .parentNode
    .insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function initApp(userInfo) {
  window.entryUser = userInfo;

  Vue.use(BootstrapVue);
  Vue.use(VueTextareaAutosize);
  Vue.use(Datatable, {locale});
  Vue.use(VueClipboard);
  Vue.use(BlockUI);
  Vue.use(Vuelidate)


  new Vue({el: '#app', router, store, template: '<App/>', components: {
    App
  }});
}
