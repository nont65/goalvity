<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users/{id}/lessons/{lesson}/verify', 'API\UserController@checkUserLesson');
Route::get('/users/{id}/lessons', 'API\UserController@getUserLesson');
Route::post('/users/{id}/lessons', 'API\UserController@addUserLesson');
Route::patch('/users/{id}/lessons', 'API\UserController@updateRecentViewLessonId');
Route::get('/users/{id}/event/register', 'API\UserController@getEventRegister');
Route::get('/users/{id}', 'API\UserController@getUserDetail');
Route::post('/event/register', 'API\UserController@submitEventRegister');
Route::post('/login', 'API\UserController@login');

Route::get('/lessons/count', 'API\LessonController@getCountLesson');
Route::post('/lessons/list', 'API\LessonController@getListLesson');
Route::patch('/lessons/{id}', 'API\LessonController@update');
Route::post('/lessons', 'API\LessonController@add');
Route::get('/lessons/alias', 'API\LessonController@getLessonAlias');
Route::get('/lessons/{alias}/all', 'API\LessonController@getLesson');
Route::get('/lessons/{alias}', 'API\LessonController@getOpenLesson');

Route::get('/students/count', 'API\StudentController@getCountStudent');
Route::post('/students/list', 'API\StudentController@getListStudent');

Route::get('/lessons/{user_id}/comments/{last_comment_id}', 'API\CommentController@getComments');
Route::get('/comments/{comment_id}', 'API\CommentController@getAnswerComments');
Route::post('/comments', 'API\CommentController@postComment');

Route::get('/events/count', 'API\EventController@getCount');
Route::post('/events/list', 'API\EventController@getListRegistered');
Route::post('/events/resend', 'API\EventController@resendEmail');
Route::post('/events/approved', 'API\EventController@setApprovedRegistered');
Route::post('/events/ticket', 'API\EventController@useTicket');
Route::get('/events/sendThankMail/{skip}', 'API\EventController@sendThankMail');

Route::post('/configs', 'API\SettingController@save');
Route::get('/configs', 'API\SettingController@getConfigSettings');

Route::post('/logs', 'API\LogController@addLog');
