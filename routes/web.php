<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/maintainance/upgrade', 'MaintainanceController@upgradeData');

Route::get('/policy', function() {
    return view('policy');
});
Route::get('/terms-and-conditions', function() {
    return view('terms-and-conditions');
});

Route::get('/index-test', function () {
  return view('index-test');
});

Route::get('/index-test/{path}', function() {
  return view('index-test');
})->where('path', '[\/\w\.-]*');

Route::get('/', function () {
    return view('index');
});

Route::get('/{path}', function() {
    return view('index');
})->where('path', '[\/\w\.-]*');
