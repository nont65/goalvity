<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_tickets', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->string('ticket_code');
            $table->longText('qrcode_data');
            $table->unsignedTinyInteger('status');
            $table->timestamp('created_at');
            $table->timestamp('used_at')->nullable();

            $table->primary(['user_id']);
            $table->index(['ticket_code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_tickets');
    }
}
