<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->unsignedTinyInteger('user_type');
            $table->unsignedBigInteger('last_comment_id')->nullable();
            $table->dateTime('last_comment_time')->nullable();
            $table->unsignedBigInteger('recent_view_lesson_id')->nullable();
            $table->dateTime('recent_view_lesson_time')->nullable();
            $table->string('profile_image')->nullable();
            $table->timestamps();
        });

        Schema::create('user_fb_mapping', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('fb_id');
            $table->timestamp('created_at');

            $table->primary(['user_id', 'fb_id']);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_fb_mapping');
        Schema::dropIfExists('users');
    }
}
