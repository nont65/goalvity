<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Nont Kung',
            'email' => 'nont65@hotmail.com',
            'user_type' => 1,
            'profile_image' => '1238499636182114.jpg',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('user_fb_mapping')->insert([
            'user_id' => 1,
            'fb_id' => 1238499636182114,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
