<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lessons')->insert([
            [
                'title' => 'Test 01',
                'alias' => 'test-01',
                'youtube_url' => 'https://www.youtube.com/embed/videoseries?list=PLnOeksYYIxDdH10iSU7YHoAk-4uBrC3mD',
                'description' => "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s')
            ], [
                'title' => 'Goal Class EP4',
                'alias' => 'goalclassep04',
                'youtube_url' => 'https://www.youtube.com/embed/videoseries?list=PLnOeksYYIxDdH10iSU7YHoAk-4uBrC3mD',
                'description' => "<p>การบ้าน</p><p>- คุณสังเกตเห็นว่าตัวเองส่วนใหญ่แล้วในชีวิตแต่ละวันใช้ความคิดเกี่ยวกับเรื่องอะไร ?</p><p>- จงจับความคิดที่สร้างผลลัพธ์ในแต่ละวันให้ได้อย่างน้อย 1 ความคิดจนครบ 7 วัน<br />Day 1:<br />Day 2:<br />Day 3:<br />Day 4:<br />Day 5:<br />Day 6:<br />Day 7:<br /></p><p>- อุปนิสัย หรือ พฤติกรรมใดบ้างของคุณที่ขัดขวางการลงมือกระทำที่ไม่สร้างผลลัพธ์ ?</p>",
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
