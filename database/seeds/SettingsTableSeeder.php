<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'setting_field_name' => 'login_video_url',
                'value' => 'https://www.youtube.com/embed/videoseries?list=PLnOeksYYIxDdH10iSU7YHoAk-4uBrC3mD'
            ]
        ]);
    }
}
